# root-suite

Pacman hooks for root users.<br>
To remove despicable root checks that break user freedom.

Place files in:
```
# Hooks ("hooks" folder)
/etc/pacman.d/hooks/
# Executables to be used by hooks ("hook.bin" folder)
/etc/pacman.d/hook.bin/
# Executables to be used by you ("bin" folder)
/usr/bin/
```

# Packaging

[root-suite](https://codeberg.org/realroot/artix-pkgbuilds/src/branch/main/root-suite/PKGBUILD)
